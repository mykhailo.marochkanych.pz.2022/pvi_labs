const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const http = require('http');

const app = express();
const server = http.createServer(app);

const initSocket = require('./socket');
const io = initSocket(server);

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

// Middleware для обробки запитів до /socket.io/
app.use('/socket.io', (req, res) => {
    res.sendStatus(404);
});

const authRoutes = require('./routes/auth');
const studentRoutes = require('./routes/students');
const chatRoutes = require('./routes/chats');

app.use('/auth', authRoutes);
app.use('/students', studentRoutes);
app.use('/chats', chatRoutes);

const port = process.env.PORT || 3000;
server.listen(port, () => {
    console.log(`Сервер запущено на http://localhost:${port}`);
});