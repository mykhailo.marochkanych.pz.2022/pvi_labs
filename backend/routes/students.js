const express = require('express');
const router = express.Router();
const db = require('../db');

// Маршрути для роботи зі студентами
router.get('/', getStudents);
router.get('/:id', getStudentById);
router.post('/addStudent', addStudent);
router.put('/updateStudent/:id', updateStudent);
router.delete('/deleteStudent/:id', deleteStudent);

// Функція для отримання всіх студентів
function getStudents(req, res) {
    console.log("getStudents")
    db.all("SELECT * FROM students", [], (err, rows) => {
        if (err) {
            res.status(500).send({ status: 'error', error: err.message });
        } else {
            res.send({ status: 'success', data: rows });
        }
    });
}

// Функція для отримання студента за id
function getStudentById(req, res) {
    console.log("getStudentById")
    const id = req.params.id;
    db.get("SELECT * FROM students WHERE id = ?", [id], (err, row) => {
        if (err) {
            res.status(500).send({ status: 'error', error: err.message });
        } else if (!row) {
            res.status(404).send({ status: 'error', error: 'Студент не знайдений' });
        } else {
            res.send({ status: 'success', data: row });
        }
    });
}

// Функція для додавання нового студента
function addStudent(req, res) {
    console.log("addStudent");
    const student = req.body;

    // Виведення даних
    console.log(student);

    db.run('INSERT INTO students (group_number, first_name, last_name, middle_name, dob, gender) VALUES (?, ?, ?, ?, ?, ?)',
        [student.group_number, student.first_name, student.last_name, student.middle_name, student.dob, student.gender],
        function(err) {
            if (err) {
                res.status(500).send({ status: 'error', error: err.message });
            } else {
                res.send({ status: 'success', data: { id: this.lastID, ...student } });
            }
        });
}

// Функція для оновлення даних студента
function updateStudent(req, res) {
    console.log("updateStudent");
    const id = req.params.id;
    const student = req.body;
    db.run('UPDATE students SET group_number = ?, first_name = ?, last_name = ?, middle_name = ?, dob = ?, gender = ? WHERE id = ?',
        [student.group_number, student.first_name, student.last_name, student.middle_name, student.dob, student.gender, id],
        function(err) {
            if (err) {
                res.status(500).send({ status: 'error', error: err.message });
            } else if (this.changes === 0) {
                res.status(404).send({ status: 'error', error: 'Студент не знайдений' });
            } else {
                res.send({ status: 'success', data: student });
            }
        });
}

// Функція для видалення студента
function deleteStudent(req, res) {
    console.log("deleteStudent");
    const id = req.params.id;
    db.run('DELETE FROM students WHERE id = ?', [id], function(err) {
        if (err) {
            res.status(500).send({ status: 'error', error: err.message });
        } else if (this.changes === 0) {
            res.status(404).send({ status: 'error', error: 'Студент не знайдений' });
        } else {
            res.send({ status: 'success', message: 'Студент успішно видалений' });
        }
    });
}

module.exports = router;