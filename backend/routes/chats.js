const express = require('express');
const router = express.Router();
//const { ChatMessage, Chat } = require('../db');
const { ChatMessage, Chat } = require('../mongo');
// Маршрути для роботи з чатами
router.post('/create-chat', createChat);
router.get('/user-chats/:userId', getUserChats);
router.get('/chat-messages/:chatId', getChatMessages);
router.post('/add-user-to-chat/:chatId/:userId', addUserToChat);

// створення чату
async function createChat(req, res) {
    try {
        const { chatName } = req.body;

        // Перевіряємо, чи існує chatName в req.body
        if (!chatName) {
            return res.status(400).send({ error: 'Назва чату не була передана' });
        }

        // Перевірка, чи існує чат із зазначеною назвою
        const existingChat = await Chat.findOne({ chatName: chatName });
        if (existingChat) {
            // Якщо чат із зазначеною назвою вже існує, повертаємо його ідентифікатор
            return res.status(200).send({ status: 'success', chatId: existingChat.chatId });
        }

        // Якщо чату з такою назвою не існує, створюємо новий
        // Якщо чату з такою назвою не існує, створюємо новий
        const newChat = new Chat({ chatName: chatName, users: [] });
        newChat.chatId = Math.floor(Math.random() * 10000000000); // Generate a random 10-digit number зробити щоб було унікальне
        await newChat.save();
        res.status(200).send({ status: 'success', chatId: newChat.chatId });
    } catch (err) {
        res.status(500).send({ error: err.message });
    }
}

// Функція для отримання списку чатів, у яких бере участь користувач
async function getUserChats(req, res) {
    const userId = req.params.userId;
    try {
        const chats = await Chat.find({ users: userId }).select('chatId chatName');
        res.status(200).send({ chats });
    } catch (err) {
        res.status(500).send({ error: err.message });
    }
}

// Функція для отримання повідомлень у вибраному чаті
async function getChatMessages(req, res) {
    const chatId = req.params.chatId;
    try {
        const messages = await ChatMessage.find({ chatId })
            .populate('senderId', 'firstName lastName')
            .select('message timestamp senderName')
            .sort('timestamp');
        res.status(200).send({ messages });
    } catch (err) {
        res.status(500).send({ error: err.message });
    }
}

// Функція для додавання користувача до чату
async function addUserToChat(req, res) {
    const chatId = req.params.chatId;
    const userId = req.params.userId;

    try {
        // Перевірка, чи вже учасник чату
        const chat = await Chat.findOne({ chatId, users: userId });
        if (chat) {
            // Користувач вже є учасником цього чату
            return res.status(200).send({ status: 'success', message: 'Користувач вже є учасником чату' });
        }

        // Додавання користувача до чату
        await Chat.updateOne({ chatId }, { $push: { users: userId } });
        const newMessage = new ChatMessage({
            chatId,
            senderId: null,
            senderName: 'Sustem',
            message: `Додано користувача`,
            timestamp: new Date(),
        });
        await newMessage.save();
        res.status(200).send({ status: 'success', message: 'Користувача додано до чату' });
    } catch (err) {
        res.status(500).send({ error: err.message });
    }
}

module.exports = router;