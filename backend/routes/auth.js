const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const db = require('../db');

// Маршрути для авторизації та реєстрації
router.post('/signup', signup);
router.post('/login', login);
router.get('/user-id/:sessionToken', getUserId);
router.get('/user-name/:userId', getUserName);


function signup(req, res) {
    console.log("signup");
    const { password, group, firstName, lastName, middleName, dob, gender } = req.body;

    // Генерація токена сесії
    const sessionToken = crypto.randomBytes(32).toString('hex');

    // Перевірка, чи існує вже користувач з такими даними
    db.get('SELECT id FROM students WHERE group_number = ? AND first_name = ? AND last_name = ? AND middle_name = ? AND dob = ? AND gender = ?',
        [group, firstName, lastName, middleName, dob, gender],
        (err, row) => {
            if (err) {
                res.status(500).send({ error: err.message });
                return;
            }

            if (row) {
                // Користувач вже існує, оновлюємо пароль та токен сесії
                db.run('UPDATE students SET password = ?, session_token = ? WHERE id = ?',
                    [password, sessionToken, row.id],
                    (err) => {
                        if (err) {
                            res.status(500).send({ error: err.message });
                        } else {
                            res.status(200).send({ status: 'success',sessionToken, userId: row.id });
                        }
                    });
            } else {
                // Користувача немає, додаємо нового
                db.run('INSERT INTO students (password, session_token, group_number, first_name, last_name, middle_name, dob, gender) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
                    [password, sessionToken, group, firstName, lastName, middleName, dob, gender],
                    function(err) {
                        if (err) {
                            res.status(500).send({ error: err.message });
                        } else {
                            res.status(200).send({ status: 'success',sessionToken, userId: this.lastID });
                        }
                    });
            }
        });
}

// Функція для авторизації
function login(req, res) {
    console.log("login");
    const { id, password } = req.body;

    db.get('SELECT * FROM students WHERE id = ?', [id], (err, row) => {
        if (err) {
            res.status(500).send({status: 'unsuccess', error: err.message });
            return;
        }

        if (!row) {
            res.status(401).send({status: 'unsuccess', error: 'Невірний ID користувача або пароль' });
            return;
        }

        // Порівняння введеного паролю з хешованим паролем у базі даних
        bcrypt.compare(password, row.password, (err, result) => {
            if (err) {
                res.status(500).send({ status: 'unsuccess',error: err.message });
                return;
            }

            if (result) {
                res.status(401).send({ status: 'unsuccess',error: 'Невірний ID користувача або пароль' });
                return;
            }

            // Генерація нового токену сеансу
            const newSessionToken = crypto.randomBytes(32).toString('hex');

            // Оновлення токену сеансу в базі даних
            db.run('UPDATE students SET session_token = ? WHERE id = ?', [newSessionToken, id], (err) => {                    //тут вибиває опмилку
                if (err) {
                    res.status(500).send({ status: 'unsuccess',error: err.message });
                } else {
                    res.status(200).send({ status: 'success',sessionToken: newSessionToken , id: id });
                }
            });
        });
    });
}

// Функція для отримання ID користувача за токеном сесії
function getUserId(req, res) {
    console.log("getUserId");
    const sessionToken = req.params.sessionToken;
    db.get('SELECT id FROM students WHERE session_token = ?', [sessionToken], (err, row) => {
        if (err) {
            res.status(500).send({ error: err.message });
        } else if (!row) {
            res.status(401).send({ error: 'Невірний токен сесії' });
        } else {
            res.status(200).send({ userId: row.id });
        }
    });
}
// Функція для отримання ID користувача за токеном сесії
function getUserName(req, res) {
    console.log("getUserName");
    const userId = req.params.userId;
    db.get('SELECT first_name FROM students WHERE id = ?', [userId], (err, row) => {
        if (err) {
            res.status(500).send({ error: err.message });
        } else if (!row) {
            res.status(401).send({ error: 'Невірний токен сесії' });
        } else {
            res.status(200).send({ userName: row.first_name });
        }
    });
}



module.exports = router;























