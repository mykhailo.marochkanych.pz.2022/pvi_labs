const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('database.db', (err) => {
    if (err) {
        console.error('Помилка при відкритті бази даних:', err.message);
    } else {
        console.log('Підключення до бази даних успішно встановлено');
        initTables();
    }
});



function initTables() {
    // Створення таблиць

    db.run(`CREATE TABLE IF NOT EXISTS students (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      password TEXT,
      session_token TEXT,
      group_number TEXT,
      first_name TEXT,
      last_name TEXT,
      middle_name TEXT,
      dob TEXT,
      gender TEXT
    )`, (err) => {
        if (err) {
            console.error('Помилка при створенні таблиці students:', err.message);
        } else {
            console.log('Таблиця "students" успішно створена');
        }
    });

    db.run(`CREATE TABLE IF NOT EXISTS chats (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT,
          created_at TEXT DEFAULT CURRENT_TIMESTAMP
        )`, (err) => {
        if (err) {
            console.error('Помилка при створенні таблиці chats:', err.message);
        } else {
            console.log('Таблиця "chats" успішно створена');
        }
    });

    db.run(`CREATE TABLE IF NOT EXISTS chat_members (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          chat_id INTEGER,
          student_id INTEGER,
          FOREIGN KEY (chat_id) REFERENCES chats(id),
          FOREIGN KEY (student_id) REFERENCES students(id)
        )`, (err) => {
        if (err) {
            console.error('Помилка при створенні таблиці chat_members:', err.message);
        } else {
            console.log('Таблиця "chat_members" успішно створена');
        }
    });

    db.run(`CREATE TABLE IF NOT EXISTS chat_messages (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          chat_id INTEGER,
          sender_id INTEGER,
          message TEXT,
          timestamp TEXT,
          FOREIGN KEY (chat_id) REFERENCES chats(id),
          FOREIGN KEY (sender_id) REFERENCES students(id)
        )`, (err) => {
        if (err) {
            console.error('Помилка при створенні таблиці chat_messages:', err.message);
        } else {
            console.log('Таблиця "chat_messages" успішно створена');
        }
    });
}
module.exports = db;