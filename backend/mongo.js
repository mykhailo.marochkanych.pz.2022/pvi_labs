const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Chats', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(() => {
        console.log('Підключення до MongoDB успішно встановлено');
    })
    .catch((err) => {
        console.error('Помилка при підключенні до MongoDB:', err.message);
    });

mongoose.connection.on('open', () => {
    console.log('Успішне підключення до MongoDB');
});

// Схема для збереження даних чатів (повідомлень)
const chatMessageSchema = new mongoose.Schema({
    chatId: Number,
    senderId: Number,
    senderName: String,
    message: String,
    timestamp: Date,
});

const ChatMessage = mongoose.model('ChatMessage', chatMessageSchema);

// Схема для збереження ідентифікаторів чатів та списку користувачів
const chatSchema = new mongoose.Schema({
    chatName: String, // Змінено назву поля на chatName
    chatId: Number,
    users: [Number],
});

const Chat = mongoose.model('Chat', chatSchema);
module.exports = {
    ChatMessage,
    Chat,
};