const socketIO = require('socket.io');
const { ChatMessage } = require('./mongo');

const init = (server) => {
    const io = socketIO(server, {
        cors: {
            origin: "*", // Дозволити з'єднання з будь-якого джерела
            methods: ["GET", "POST"] // Дозволені методи HTTP
        }
    });

    // Обробник з'єднання WebSocket
    io.on('connection', (socket) => {
        console.log('New client connected');

        // Приєднання до кімнати чату
        socket.on('join-room', (chatId) => {
            socket.join(chatId);
            console.log(`Client joined room: ${chatId}`);
        });

        // Відправка повідомлення
        socket.on('send-message', async ({ chatId, message, senderId, senderName }) => {
            console.log('New message:', message);
            const timestamp = new Date();

            try {
                // Зберігання повідомлення в базі даних MongoDB
                const newMessage = new ChatMessage({
                    chatId,
                    senderId,
                    senderName,
                    message,
                    timestamp
                });
                await newMessage.save();

                // Розсилка повідомлення учасникам кімнати
                io.to(chatId).emit('new-message', { message, senderId, senderName, timestamp: timestamp.toISOString() });
            } catch (err) {
                console.error('Error saving message:', err);
            }
        });


        // Обробник відключення клієнта
        socket.on('disconnect', () => {
            console.log('Client disconnected');
        });
    });

    return io;
}

module.exports = init;