
var isEditMode = false;
var studentId= null;

function deleteStudent(button) {
    var studentId = button.getAttribute('data-student-id');
    if (studentId) {
        Swal.fire({
            icon: 'warning',
            title: 'Ви впевнені?',
            text: "Ви дійсно хочете видалити цього студента?",
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Так, видалити!',
            cancelButtonText: 'Скасувати'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'DELETE',
                    url: 'http://localhost:3000/students/deleteStudent/' + studentId,
                    success: function(response) {
                        console.log('Відповідь сервера:', response);
                        if (response.status === 'success') {
                            // Оновлюємо дані про студентів з відповіді сервера
                            displayStudentsData(response.data);
                            button.parentNode.parentNode.remove(); // Видаляємо рядок з таблиці
                            Swal.fire({
                                icon: 'success',
                                title: 'Студента видалено',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Помилка видалення студента',
                                text: response.message
                            });
                        }
                    },
                    error: function(xhr, status, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Помилка видалення студента',
                            text: error
                        });
                    }
                });
            }
        });
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Помилка',
            text: 'Не вдається знайти ідентифікатор студента для видалення.'
        });
    }
}

function editStudent(button) {
    studentId = button.getAttribute('data-student-id');
    if (studentId) {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:3000/students/' + studentId,
            success: function(response) {
                if (response.status === 'success') {
                    isEditMode = createStudentForm(true, response.data);
                } else {
                    console.error('Помилка отримання даних про студента:', response.message);
                }
            },
            error: function(xhr, status, error) {
                console.error('Помилка отримання даних про студента:', error);
            }
        });
    } else {
        alert('Помилка: не вдається знайти ідентифікатор студента для редагування.');
    }
}

function displayStudentsData(studentsData) {
    console.log('Дані про студентів:', studentsData);
    $('#studentTableBody').empty();
    studentsData.forEach(function(student) {
        if (student.group_number && student.first_name && student.last_name && student.middle_name && student.gender && student.dob) {
            var newRow = $('<tr>');

            newRow.append('<td><input type="checkbox"></td>');
            newRow.append('<td>' + student.group_number + '</td>');
            newRow.append('<td>' + student.first_name + '</td>');
            newRow.append('<td>' + student.last_name + '</td>');
            newRow.append('<td>' + student.middle_name + '</td>');
            newRow.append('<td>' + student.gender + '</td>');
            newRow.append('<td>' + student.dob + '</td>');
            newRow.append('<td><span class="status-circle status-unchecked"></span></td>');
			newRow.append('<td><button class="Student-btn" data-student-id="' + student.id + '"onclick="deleteStudent(this)"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/><path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/></svg></button><button class="editStudent-btn" data-student-id="' + student.id + '" onclick="editStudent(this)"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16"><path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325"/></svg></button></td>');

			$('#studentTableBody').append(newRow);
        } else {
            console.error('Некоректний формат даних про студента:', student);
        }
    });
}
function updateStudentData(studentId, group, firstName, lastName, middleName, dob, gender) {
    // Перевіряємо, чи всі обов'язкові поля заповнені
    if (!group || !firstName || !lastName || !middleName || !dob || !gender) {
        Swal.fire({
            icon: 'error',
            title: 'Помилка',
            text: "Будь ласка, заповніть всі обов'язкові поля"
        });
        return;
    }

    // Видаляємо пробіли на початку та в кінці рядків
    group = group.trim();
    firstName = firstName.trim();
    lastName = lastName.trim();
    middleName = middleName.trim();
    gender = gender.trim();

    var formattedDOB = formatDOB(dob);
    var studentData = {
        group_number: group,
        first_name: firstName,
        last_name: lastName,
        middle_name: middleName,
        dob: formattedDOB,
        gender: gender
    };

    $.ajax({
        type: 'PUT',
        url: 'http://localhost:3000/students/updateStudent/' + studentId,
        data: JSON.stringify(studentData),
        contentType: 'application/json',
        success: function(response) {
            if (response.status === 'success') {
                console.log('Дані студента успішно оновлені:', response.message);
                getStudentsData(); // Оновлюємо таблицю студентів після успішного оновлення даних
            } else {
               Swal.fire({
                    icon: 'error',
                    title: 'Помилка оновлення даних студента',
                    text: response.message
                });
            }
        },
        error: function(xhr, status, error) {
            Swal.fire({
                icon: 'error',
                title: 'Помилка оновлення даних студента',
                text: error
            });
        }
    });

    // Закриття діалогового вікна
    $('#dialog').dialog('close');
}

// Функція для отримання даних про студентів з сервера
function getStudentsData() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/students',
        success: function(response) {
            console.log('Статус відповіді:', response.status);
            if (response.status === 'success') {
                console.log('Дані про студентів успішно отримано:', response.data);
                displayStudentsData(response.data);
            } else {
                console.error('Помилка отримання даних про студентів:', response.message);
            }
        },
        error: function(xhr, status, error) {
            console.error('Помилка виконання запиту:', error);
        }
    });
}

function formatDOB(dobString) {
    var dobComponents = dobString.split('.');
    return dobComponents[2]+ '-' + dobComponents[1] + '-' + dobComponents[0];
}

function createStudentForm(isEditMode, studentData) {
    var form = $('<form>', { id: 'studentForm', style: 'overflow-y: auto;' }); // Змінено розміри форми

    // Створюємо функцію для додавання полів до форми
    function addFormField(labelText, inputId, inputName, inputType, value = '') {
        var label = $('<label>', { for: inputId, text: labelText });
        var input = $('<input>', { type: inputType, id: inputId, name: inputName, value: value, required: true });
        var field = $('<div>').append(label, input);
        form.append(field, '<br><br>');
    }

    // Додаємо елементи форми
    addFormField('Група:', 'group', 'group', 'text', isEditMode ? studentData.group_number : '');
    addFormField('Ім\'я:', 'firstName', 'firstName', 'text', isEditMode ? studentData.first_name : '');
    $('#firstName').on('input', function() {
        this.value = this.value.replace(/[^а-яіїєА-ЯІЇЄ']/g, '');
    });
    addFormField('Прізвище:', 'lastName', 'lastName', 'text', isEditMode ? studentData.last_name : '');
    $('#lastName').on('input', function() {
        this.value = this.value.replace(/[^а-яіїєА-ЯІЇЄ']/g, '');
    });
    addFormField('По батькові:', 'middleName', 'middleName', 'text', isEditMode ? studentData.middle_name : '');
    $('#middleName').on('input', function() {
        this.value = this.value.replace(/[^а-яіїєА-ЯІЇЄ']/g, '');
    });
    if (isEditMode) {
        // Розділіть дату народження на окремі компоненти (день, місяць, рік)
        var dobComponents = studentData.dob.split('.');
        var dobDay = dobComponents[0];
        var dobMonth = dobComponents[1];
        var dobYear = dobComponents[2];

        // Сформуйте дату у форматі "дд.мм.рррр" та встановіть її значення у полі введення дати
        var formattedDOB = dobDay + '.' + dobMonth + '.' + dobYear;
        $('#dob').attr('value', formattedDOB);
    }

    addFormField('Дата народження:', 'dob', 'dob', 'date', isEditMode ? formatDOB(studentData.dob) : '');

    if (isEditMode) {
        // Розділіть дату народження на окремі компоненти (день, місяць, рік)
        var dobComponents = studentData.dob.split('.');
        var dobDay = dobComponents[0];
        var dobMonth = dobComponents[1];
        var dobYear = dobComponents[2];

        // Сформуйте дату у форматі "дд.мм.рррр" та встановіть її значення у полі введення дати
        var formattedDOB = dobDay + '.' + dobMonth + '.' + dobYear;
        $('#dob').attr('value', formattedDOB);
    }

    var genderOptions = [' ','Чоловіча', 'Жіноча', 'Інша'];
    var genderSelect = $('<select>', { id: 'gender', name: 'gender', required: true, style: 'flex: 2;' });
    genderOptions.forEach(function(option) {
        genderSelect.append($('<option>', { value: option, text: option }));
    });
    if (isEditMode) genderSelect.val(studentData.gender);
    var genderField = $('<div>', { style: 'display: flex; flex-direction: row; justify-content: space-between; align-items: baseline;' }).append($('<label>', { for: 'gender', text: 'Стать:', style: 'flex: 1;' }), genderSelect);
    form.append(genderField, '<br><br>');


    $('#group').on('input', function() {
        if (!/^[A-ZА-ЯІЇЄ0-9-]+$/.test($(this).val())) {
            $(this).val($(this).val().replace(/[^A-ZА-ЯІЇЄ0-9-]/g, ''));
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: 'Група може містити лише великі літери, цифри та знак "-"'
            });
        }
    });

    $('#firstName, #lastName, #middleName').on('input', function() {
        if (!/^[A-ZА-ЯІЇЄ][a-zA-ZА-ЯІЇЄа-яіїє'-]*-?[A-ZА-ЯІЇЄ]?[a-zA-ZА-ЯІЇЄа-яіїє'-]*$/.test($(this).val())) {
            $(this).val($(this).val().replace(/[^a-zA-ZА-ЯІЇЄа-яіїє'-]/g, ''));
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: "Ім'я, прізвище та по батькові повинні починатися з великої літери і містити лише букви, знаки \"-\" та \"'\""
            });
        }
    });

    // Додаємо створену форму до діалогового вікна і відкриваємо його
    $('#dialog').html(form);
    $("#dialog").dialog("open");

    return isEditMode;
}


function addStudentToTable(group, firstName, lastName, middleName, dob, gender) {
    var formattedDOB = formatDOB(dob);
    var studentData = {
        group_number: group,
        first_name: firstName,
        last_name: lastName,
        middle_name: middleName, // Змінено middleName на middle_name
        dob: formattedDOB,
        gender: gender
    };

    $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/students/addStudent',
        data: JSON.stringify(studentData),
        contentType: 'application/json',
        success: function(response) {
            console.log('Студент успішно доданий до бази даних:', response);
            localStorage.setItem("usetId",response.userId)
            getStudentsData(); // Оновлюємо табличку після додавання студента
        },
        error: function(xhr, status, error) {
            Swal.fire({
                icon: 'error',
                title: 'Помилка додавання студента',
                text: error
            });
        }
    });

    // Очищення форми
    $('#studentForm')[0].reset();

    // Закриття діалогового вікна
    $('#dialog').dialog('close');
}


$(document).ready(function() {
    // Функція для додавання студента до таблиці

    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        width: '410px',
        //height: 'auto',
        buttons: {
            "Зберегти": function() {
                // Отримання даних з форми
                var group = $('#group').val().toUpperCase(); // Конвертуємо великі літери
                var firstName = $('#firstName').val().trim(); // Видаляємо зайві пробіли
                var lastName = $('#lastName').val().trim();
                var middleName = $('#middleName').val().trim();
                var dobInput = $('#dob').val(); // Отримуємо дату введену користувачем
                var dob = formatDOB(dobInput);
                // Розбиваємо дату на компоненти (рік, місяць, день)
                var dobComponents = dobInput.split('-');
                var dob = new Date(dobComponents[0], dobComponents[1] - 1, dobComponents[2]); // Місяць - 1, бо в JavaScript місяці починаються з 0

                // Форматуємо дату у вказаний формат (дд.мм.рр)
                var formattedDOB = dob.getDate().toString().padStart(2, '0') + '.' + (dob.getMonth() + 1).toString().padStart(2, '0') + '.' + dob.getFullYear().toString();

                var gender = $('#gender').val();

                // Валідація даних
                var isValid = true;
                var currentYear = new Date().getFullYear();
                var minDOB = new Date(currentYear - 16, 0, 1); // 16 років назад

                if (!/^[A-ZА-ЯІЇЄ0-9-]+$/.test(group)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Помилка',
                        text: 'Група може містити лише великі літери, цифри та знак "-"'
                    });
                    isValid = false;
                }

                if (!/^[A-ZА-ЯІЇЄ][a-zA-ZА-ЯІЇЄа-яіїє'-]*-?[A-ZА-ЯІЇЄ]?[a-zA-ZА-ЯІЇЄа-яіїє'-]*$/.test(firstName)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Помилка',
                        text: "Ім'я повинно починатися з великої літери та може містити лише букви, знаки - та '"
                    });
                    isValid = false;
                }

                if (!/^[A-ZА-ЯІЇЄ][a-zA-ZА-ЯІЇЄа-яіїє'-]*-?[A-ZА-ЯІЇЄ]?[a-zA-ZА-ЯІЇЄа-яіїє'-]*$/.test(lastName)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Помилка',
                        text: "Прізвище повинно починатися з великої літери та може містити лише букви, знаки - та '"
                    });
                    isValid = false;
                }

                if (!/^[A-ZА-ЯІЇЄ][a-zA-ZА-ЯІЇЄа-яіїє'-]*-?[A-ZА-ЯІЇЄ]?[a-zA-ZА-ЯІЇЄа-яіїє'-]*$/.test(middleName)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Помилка',
                        text: "По батькові повинно починатися з великої літери та може містити лише букви, знаки - та ' "
                    });
                    isValid = false;
                }

                if (dob >= minDOB) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Помилка',
                        text: "Дата народження повинна бути не меншою за 16 років."
                    });
                    isValid = false;
                }

                if (isValid) {
                    // Якщо режим редагування, оновлюємо дані студента
                    if (isEditMode) {
                        updateStudentData(studentId, group, firstName, lastName, middleName, formattedDOB, gender); // Передаємо відформатовану дату
                    } else {
                        // Додавання студента до таблиці
                        addStudentToTable(group, firstName, lastName, middleName, formattedDOB, gender); // Передаємо відформатовану дату
                    }
                    $(this).dialog("close");
                }
            },
            Скасувати: function() {
                $(this).dialog("close");
            }
        }
    });

    // При натисканні на кнопку "addStudentForm-btn"
    $("#addStudentForm-btn").click(function() {
        isEditMode = createStudentForm(false, null);

        getStudentsData();
	});


    getStudentsData();


});



