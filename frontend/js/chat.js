// Підключення до WebSocket-сервера

const socket = io('http://localhost:3000');

// Отримання ID користувача з локального сховища
var userId = localStorage.getItem('userId');
var userName = getUserName();
//var userName= null;

var userName= null;
var sessionToken = null;

var currentChatId = null;



// Отримання ID користувача за його токеном сеансу
function getUserID() {
    $.ajax({
        url: `http://localhost:3000/auth/user-id/${localStorage.getItem('sessionToken')}`,
        method: 'GET',
        success: function(data) {
            localStorage.setItem('userId', data.id);
        },
        error: function(error) {
            console.error('Помилка отримання ID користувача:', error);
        }
    });
}

function getUserName() {
    const userId = localStorage.getItem('userId');
    const response = $.ajax({
        url: `http://localhost:3000/auth/user-name/${userId}`,
        method: 'GET',
        async: false // Робимо запит синхронним
    });

    const userName = response.responseJSON.userName;
    localStorage.setItem('userName', userName);

    const nameElement = $('.name-l');
    if (userName) {
        nameElement.text(userName);
    } else {
        nameElement.text('Користувач');
    }

    return userName;
}

function createNewChat() {
    Swal.fire({
        title: 'Створити новий чат',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Створити',
        showLoaderOnConfirm: true,
        preConfirm: (chatName) => {
            if (!chatName) {
                Swal.showValidationMessage('Поле не може бути порожнім');
            } else {
                // Відправлення AJAX запиту для створення нового чату
                return $.ajax({
                   url: 'http://localhost:3000/chats/create-chat',
                    method: 'POST',
                    contentType: 'application/json', // Додаємо тип контенту
                    data: JSON.stringify({ chatName: chatName }),
                })
                    .then(function(data) {
                        if (data.status === 'success') {
                            // Успішне створення чату
                            console.log('Чат успішно створено з ідентифікатором:', data.roomId);
                            currentChatId = data.chatId; // Зберігаємо ID нового чату
                            // Додаткові дії після створення чату
                            addUserToChat(data.chatId,userId);
                        } else {
                            throw new Error('Помилка створення чату: ' + data.error);
                        }
                    })
                    .catch(function(error) {
                        console.error('Помилка створення чату:', error);
                        Swal.showValidationMessage(error.message);
                    });
            }
        },
        allowOutsideClick: () => !Swal.isLoading()
    });
}

// Функція для додавання користувача до чату
function addUserToChat(chatId, userId) {
    $.ajax({
        url: `http://localhost:3000/chats/add-user-to-chat/${chatId}/${userId}`,
        method: 'POST',
        success: function(data) {
            if (data.status === 'success') {
                console.log('Студента успішно додано до чату');
                loadUserChats(); // Перезавантаження списку чатів
                // Додаткові дії після додавання студента до чату


            } else {
                console.error('Помилка додавання студента до чату:', data.message);
            }
        },
        error: function(error) {
            console.error('Помилка додавання студента до чату:', error);
        }
    });
}

// Логіка додавання студента до поточного чату
function addUserToCurrentChat() {
    if (!currentChatId) {
        Swal.fire('Помилка', 'Спочатку виберіть чат', 'error');
        return;
    }

    Swal.fire({
        title: 'Додати студента до чату',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Додати',
        showLoaderOnConfirm: true,
        preConfirm: (newUserId) => {
            if (!newUserId) {
                Swal.showValidationMessage('Поле не може бути порожнім');
            } else {
                addUserToChat(currentChatId, newUserId);
            }
        },
        allowOutsideClick: () => !Swal.isLoading()
    });
}


// Функція для завантаження чатів користувача
function loadUserChats() {
    $.ajax({
        url: `http://localhost:3000/chats/user-chats/${userId}`,
        method: 'GET',
        success: function(data) {
            const chatList = $('.chat-list ul');
            chatList.empty(); // Очищення попереднього списку чатів
            currentChatId = data.chats.length > 0 ? data.chats[0].chatId : null;
            if (data.chats.length === 0) {
                chatList.append('<li>У вас немає чатів</li>');
            } else {
                $.each(data.chats, function(index, chat) {
                    const chatItem = $('<li></li>');
                    const chatLink = $(`<a href="#" data-chat-id="${chat.chatId}">${chat.chatName}</a>`);
                    chatLink.click(function() {
                        loadChatMessages(chat.chatId);
                    });
                    chatItem.append(chatLink);
                    chatList.append(chatItem);
                });
            }
        },
        error: function(error) {
            console.error('Помилка завантаження списку чатів:', error);
        }
    });
}

// Функція для завантаження повідомлень у вибраному чаті
function loadChatMessages(chatId) {
    $.ajax({
        url: `http://localhost:3000/chats/chat-messages/${chatId}`,
        method: 'GET',
        success: function(data) {
            const messageContainer = $('.chat-messages');
            messageContainer.empty(); // Очищення попередніх повідомлень

            if (data.messages.length === 0) {
                messageContainer.append('<div>Немає повідомлень у цьому чаті</div>');
            } else {
                $.each(data.messages, function(index, message) {
                    const messageElement = $('<div></div>').addClass('message');
                    if (message.senderId === Number(userId)) {
                        messageElement.addClass('sent');
                    } else {
                        messageElement.addClass('received');
                    }
                    messageElement.html(`
                        <span class="message-author">${message.senderName}:</span>
                        <span class="message-text">${message.message}</span>
                        <span class="message-timestamp">${new Date(message.timestamp).toLocaleString()}</span>
                    `);
                    messageContainer.append(messageElement);
                });
            }
        },
        error: function(error) {
            console.error('Помилка завантаження повідомлень:', error);
        }
    });
    currentChatId = chatId;
    // Приєднання до кімнати чату
    socket.emit('join-room', chatId);
}




/*async function sendMessage(event) {
    event.preventDefault();*/

async function sendMessage() {
    const input = $('#message-input');
    const message = input.val().trim();

    if (!message) {
        Swal.fire('Помилка', 'Повідомлення не може бути порожнім', 'error');
        return;
    }

    if (!currentChatId) {
        Swal.fire('Помилка', 'Спочатку виберіть чат', 'error');
        return;
    }


    // Відправлення повідомлення на сервер
    socket.emit('send-message', { chatId: currentChatId, message, senderId: userId, senderName: typeof userName === 'string' ? userName : 'Unknown' });


    // Очищення поля вводу
    input.val('');
    loadChatMessages(currentChatId);
}



// Отримання нового повідомлення
socket.on('new-message', ({ message, senderId, senderName, timestamp }) => {
    const messageContainer = $('.chat-messages');
    const messageElement = $('<div></div>').addClass('message');
    if (senderId === userId) {
        messageElement.addClass('sent');
    } else {
        messageElement.addClass('received');
    }
    messageElement.html(`
        <span class="message-author">${senderName}:</span>
        <span class="message-text">${message}</span>
        <span class="message-timestamp">${new Date(timestamp).toLocaleString()}</span>
    `);
    messageContainer.append(messageElement);
    // Прокручуємо вміст контейнеру повідомлень вниз
    messageContainer.scrollTop(messageContainer.prop('scrollHeight'));
});
// Завантаження списку чатів користувача після завантаження сторінки
$(document).ready(function() {
    // Перевірка наявності userId або токена сеансу
    let userId = localStorage.getItem('userId');
    let sessionToken = localStorage.getItem('sessionToken');

    if (!userId || !sessionToken) {
        // Перенаправлення на сторінку входу, якщо userId або токена сеансу немає
        window.location.href = 'login.html';
        return;
    }

    // Отримуємо ім'я користувача перед виконанням іншого коду
    userName = getUserName();


    loadUserChats(currentChatId);
    loadChatMessages();
});
