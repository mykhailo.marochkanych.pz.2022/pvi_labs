$(document).ready(function() {
    $('#loginForm').submit(function(event) {
        event.preventDefault(); // Запобігання стандартному перезавантаженню сторінки при відправці форми

        const id = $('#id').val();
        const password = $('#password').val();

        // Створення об'єкта з даними для надсилання на сервер
        const data = {
            id: id,
            password: password
        };

        // Надсилання POST-запиту на сервер для авторизації
        $.ajax({
            type: 'POST',
            url: 'http://localhost:3000/auth/login',
            data: JSON.stringify(data),
            contentType: "application/json", // Виправлено
            dataType: "json",
            success: function(response) {
                if (response.status === 'success') {
                    // Успішна авторизація
                    // Збереження токена сесії (наприклад, в localStorage)
                    localStorage.setItem('sessionToken', response.sessionToken);
                    localStorage.setItem("userId", response.id);

                    window.location.href = 'chat.html';
                } else {
                    // Помилка авторизації
                    alert(response.message);
                }
            },
            error: function(xhr, status, error) {
                // Обробка помилки AJAX-запиту
                console.error('Помилка AJAX-запиту:', error);
                alert('Сталася помилка при авторизації. Спробуйте пізніше.');
            }
        });
    });
});