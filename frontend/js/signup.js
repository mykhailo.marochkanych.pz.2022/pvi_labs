$(document).ready(function() {
    $("#loginForm").submit(function(event) {
        event.preventDefault();

        const formData = {
            password: $("#password").val(),
            group: $("#group").val(),
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            middleName: $("#middleName").val(),
            dob: $("#dob").val(),
            gender: $("#gender").val()
        };

        
        $.ajax({
            type: "POST",
            url: "http://localhost:3000/auth/signup",
            data: JSON.stringify(formData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if (data.status === "success") {
                    alert("Реєстрація пройшла успішно!");
                    console.log("Дані від сервера:", data);
                    localStorage.setItem("userId", $("#id").val());
                    localStorage.setItem("sessionToken", data.sessionToken);

                    window.location.href = "chat.html";
                } else {
                    alert("Помилка реєстрації: " + data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Помилка реєстрації: " + jqXHR.responseJSON.message);
            }
        });
    });
});