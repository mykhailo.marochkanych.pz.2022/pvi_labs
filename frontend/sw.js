'use strict';
importScripts('sw-toolbox.js');
toolbox.precache(['dashboard.html', 'tasks.html','students.html','index.html', '/js/main.js', '/js/students.js','/js/dashboard.js','/js/tasks.js', '/css/main.css','/css/dashboard.css', '/css/students.css', '/css/tasks.css']);
toolbox.router.get('/icons/*', toolbox.cacheFirst);
toolbox.router.get('/*', toolbox.networkFirst, { networkTimeoutSeconds: 5 });
